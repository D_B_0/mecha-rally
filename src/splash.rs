use bevy::prelude::*;

use crate::{despawn, AppState, TEXT_COLOR};

pub fn plugin(app: &mut App) {
    app.add_systems(OnEnter(AppState::Splash), setup)
        .add_systems(Update, countdown.run_if(in_state(AppState::Splash)))
        .add_systems(OnExit(AppState::Splash), despawn::<OnSplashScreen>);
}

#[derive(Component)]
struct OnSplashScreen;

#[derive(Resource, Deref, DerefMut)]
struct SplashTimer(Timer);

fn setup(
    mut commands: Commands,
    // asset_server: Res<AssetServer>
) {
    commands
        .spawn((
            NodeBundle {
                style: Style {
                    align_items: AlignItems::Center,
                    justify_content: JustifyContent::Center,
                    width: Val::Percent(100.0),
                    height: Val::Percent(100.0),
                    ..default()
                },
                ..default()
            },
            OnSplashScreen,
        ))
        .with_children(|parent| {
            parent.spawn(TextBundle {
                text: Text::from_section(
                    "Mecha\nRally",
                    TextStyle {
                        // font: asset_server.load("fonts/RobotoMono-Bold.ttf"),
                        font_size: 100.0,
                        color: TEXT_COLOR,
                        ..default()
                    },
                ),
                ..Default::default()
            });
        });
    commands.insert_resource(SplashTimer(Timer::from_seconds(1.0, TimerMode::Once)))
}

fn countdown(
    mut app_state: ResMut<NextState<AppState>>,
    time: Res<Time>,
    mut timer: ResMut<SplashTimer>,
) {
    if timer.tick(time.delta()).finished() {
        app_state.set(AppState::StartMenu);
    }
}
