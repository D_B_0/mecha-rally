use bevy::{input::common_conditions::input_just_pressed, math::vec3, prelude::*, sprite::Anchor};
use rand::seq::SliceRandom;
use rand::thread_rng;
use strum::IntoEnumIterator;

use crate::{AppState, Cursor};

use super::{GameSettings, GameState, Move, MoveEvent, Player, Tank};

pub fn plugin(app: &mut App) {
    app.insert_resource(MovementTimer(Timer::from_seconds(
        1.5,
        TimerMode::Repeating,
    )))
    .add_systems(OnEnter(AppState::Game), setup)
    .add_systems(OnEnter(GameState::Preparing), draw_cards)
    .add_systems(OnExit(GameState::Preparing), reset_card_scale)
    .add_systems(OnEnter(GameState::End), handle_end_phase)
    .add_systems(
        Update,
        (
            hand_position,
            manage_hand_card_position,
            card_hitbox,
            (
                card_scale_on_hover,
                submit_turn,
                card_click.run_if(input_just_pressed(MouseButton::Left)),
            )
                .run_if(in_state(GameState::Preparing)),
            send_move_events.run_if(in_state(GameState::Movement)),
            handle_scale_change,
        )
            .run_if(in_state(AppState::Game)),
    );
}

#[derive(Component)]
struct Hand;

#[derive(Component)]
struct SelectedCards(Vec<Entity>);

#[derive(Component)]
struct CardKind(Move);

#[derive(Component)]
struct CardSelected;

#[derive(Component)]
struct DesiredScale(Vec3);

#[derive(Component)]
struct CardHitbox(Rect);

#[derive(Component)]
struct Deck(Vec<Move>);

#[derive(Component)]
struct CardsTextureAtlasHandles;

#[derive(Resource)]
struct MovementTimer(Timer);

fn spawn_card(
    mut commands: Commands,
    card_move: Move,
    texture: Handle<Image>,
    texture_atlas_layout: Handle<TextureAtlasLayout>,
) -> Entity {
    commands
        .spawn((
            CardKind(card_move),
            CardHitbox(Rect::default()),
            SpatialBundle::default(),
        ))
        .with_children(|commands| {
            commands.spawn((
                SpriteSheetBundle {
                    texture,
                    atlas: TextureAtlas {
                        layout: texture_atlas_layout,
                        index: match card_move {
                            Move::ForwardOne => 0,
                            Move::ForwardTwo => 1,
                            Move::ForwardThree => 2,
                            Move::Back => 3,
                            Move::RotateRight => 4,
                            Move::RotateLeft => 5,
                            Move::Rotate180 => 6,
                            Move::Again => 7,
                            Move::PowerUp => 8,
                        },
                    },
                    sprite: Sprite {
                        anchor: Anchor::BottomCenter,
                        ..default()
                    },
                    ..default()
                },
                DesiredScale(vec3(1.0, 1.0, 1.0)),
            ));
        })
        .id()
}

fn handle_end_phase(mut game_state: ResMut<NextState<GameState>>) {
    game_state.set(GameState::Preparing);
}

fn setup(
    mut commands: Commands,
    game_settings: Res<GameSettings>,
    asset_server: Res<AssetServer>,
    mut texture_atlas_layouts: ResMut<Assets<TextureAtlasLayout>>,
) {
    // spawn hand
    commands.spawn((
        SpatialBundle {
            transform: Transform::from_xyz(0.0, 0.0, 10.0),
            ..default()
        },
        SelectedCards(vec![]),
        Player(0),
        Deck({
            let mut vec = vec![];
            for card_move in Move::iter() {
                vec.append(&mut vec![
                    card_move;
                    (game_settings.deck_start_composition)(card_move)
                ]);
            }
            vec.shuffle(&mut thread_rng());
            vec
        }),
        Hand,
    ));
    // load cards texture atlas
    commands.spawn((
        asset_server.load::<Image>("cards.png"),
        texture_atlas_layouts.add(TextureAtlasLayout::from_grid(
            Vec2::new(150.0, 210.0),
            9,
            1,
            None,
            None,
        )),
        CardsTextureAtlasHandles,
    ));
    // setup UI elements
    let ui_root = commands
        .spawn(NodeBundle {
            style: Style {
                // bottom: Val::Percent(1.0),
                // right: Val::Percent(1.0),
                // top: Val::Auto,
                // left: Val::Auto,
                position_type: PositionType::Absolute,
                // position it at the top-right corner
                // 1% away from the top window edge
                right: Val::Percent(1.),
                bottom: Val::Percent(1.),
                // set bottom/left to Auto, so it can be
                // automatically sized depending on the text
                top: Val::Auto,
                left: Val::Auto,
                ..default()
            },
            z_index: ZIndex::Global(i32::MAX),
            ..default()
        })
        .id();
    let arrow_button = commands
        .spawn(
            ButtonBundle {
                style: Style {
                    height: Val::Px(100.0),
                    align_items: AlignItems::Center,
                    justify_content: JustifyContent::Center,
                    ..default()
                },
                image: UiImage::new(asset_server.load("arrow.png")),
                ..default()
            },
            // TextBundle {
            //     // use two sections, so it is easy to update just the number
            //     text: Text::from_sections([
            //         TextSection {
            //             value: "FPS: ".into(),
            //             style: TextStyle {
            //                 font_size: 16.0,
            //                 color: Color::WHITE,
            //                 // if you want to use your game's font asset,
            //                 // uncomment this and provide the handle:
            //                 // font: my_font_handle
            //                 ..default()
            //             },
            //         },
            //         TextSection {
            //             value: " N/A".into(),
            //             style: TextStyle {
            //                 font_size: 16.0,
            //                 color: Color::WHITE,
            //                 // if you want to use your game's font asset,
            //                 // uncomment this and provide the handle:
            //                 // font: my_font_handle
            //                 ..default()
            //             },
            //         },
            //     ]),
            //     ..Default::default()
            // },
        )
        .id();
    commands.entity(ui_root).add_child(arrow_button);
}

fn draw_cards(
    mut commands: Commands,
    mut hand_q: Query<(Entity, &mut Deck), With<Hand>>,
    cards_texture_atlas_layouts_q: Query<
        (&Handle<Image>, &Handle<TextureAtlasLayout>),
        With<CardsTextureAtlasHandles>,
    >,
    game_settings: Res<GameSettings>,
) {
    let (texture, texture_atlas_layout) = cards_texture_atlas_layouts_q.single();
    for (hand, mut deck) in &mut hand_q {
        for _ in 0..game_settings.hand_size.get() {
            let card = spawn_card(
                commands.reborrow(),
                deck.0.pop().expect("could not draw a full hand"),
                texture.clone(),
                texture_atlas_layout.clone(),
            );
            commands.entity(hand).add_child(card);
        }
    }
}

fn hand_position(
    mut hand_q: Query<&mut Transform, With<Hand>>,
    camera_q: Query<&OrthographicProjection>,
) {
    let camera = camera_q.single();
    for mut transform in &mut hand_q {
        transform.translation.x = (camera.area.max.x + camera.area.min.x) / 2.0;
        transform.translation.y = camera.area.min.y;
    }
}

fn manage_hand_card_position(
    hand_q: Query<&Children, With<Hand>>,
    mut card_q: Query<(&mut Transform, &CardHitbox, Option<&CardSelected>), With<CardKind>>,
) {
    let padding = 20.0;
    for cards in &hand_q {
        let num_cards = cards.len();
        for (index, &card) in cards.iter().enumerate() {
            let (mut transform, hitbox, selected) = card_q.get_mut(card).unwrap();
            transform.translation.x =
                (index as f32 - num_cards as f32 / 2.0 + 0.5) * (hitbox.0.width() + padding);
            transform.translation.y = if selected.is_some() { 20.0 } else { 0.0 };
        }
    }
}

fn card_scale_on_hover(
    card_q: Query<(&CardHitbox, &Children), With<CardKind>>,
    mut desired_scale_q: Query<&mut DesiredScale>,
    cursor: Res<Cursor>,
) {
    for (hitbox, children) in &card_q {
        let mut desired_scale = desired_scale_q.get_mut(*children.first().unwrap()).unwrap();
        if hitbox.0.contains(cursor.0) {
            desired_scale.0 = vec3(1.1, 1.1, 1.0);
        } else {
            desired_scale.0 = vec3(1.0, 1.0, 1.0);
        }
    }
}

fn reset_card_scale(
    card_q: Query<&Children, With<CardKind>>,
    mut desired_scale_q: Query<&mut DesiredScale>,
) {
    for children in &card_q {
        let mut desired_scale = desired_scale_q.get_mut(*children.first().unwrap()).unwrap();
        desired_scale.0 = vec3(1.0, 1.0, 1.0);
    }
}

fn card_hitbox(
    mut card_q: Query<(&GlobalTransform, &Children, &mut CardHitbox), With<CardKind>>,
    atlas_q: Query<&TextureAtlas>,
    texture_atlas_layouts: Res<Assets<TextureAtlasLayout>>,
) {
    for (global_transform, children, mut card_hitbox) in &mut card_q {
        let texture_atlas = atlas_q.get(*children.first().unwrap()).unwrap();
        let Some(image_dimensions) = texture_atlas_layouts
            .get(texture_atlas.layout.clone())
            .map(|layout| layout.textures[texture_atlas.index].size())
        else {
            continue;
        };
        let mut image_center = global_transform.translation().truncate();
        image_center.y += image_dimensions.y / 2.0;
        let bounding_box = Rect::from_center_size(image_center, image_dimensions);
        card_hitbox.0 = bounding_box;
    }
}

fn card_click(
    mut commands: Commands,
    mut hand_q: Query<(&mut SelectedCards, &Children), With<Hand>>,
    card_q: Query<(&CardHitbox, Option<&CardSelected>)>,
    cursor: Res<Cursor>,
) {
    for (mut selected_cards, hand_children) in &mut hand_q {
        for &card in hand_children {
            let (hitbox, selected) = card_q.get(card).unwrap();
            if !hitbox.0.contains(cursor.0) {
                continue;
            }
            if selected.is_none() {
                commands.entity(card).insert(CardSelected);
                selected_cards.0.push(card);
            } else {
                commands.entity(card).remove::<CardSelected>();
                if let Some(index) = selected_cards.0.iter().position(|&_card| _card == card) {
                    selected_cards.0.remove(index);
                }
            }
        }
    }
}

fn submit_turn(
    input: Res<ButtonInput<KeyCode>>,
    hand_q: Query<&SelectedCards, With<Hand>>,
    mut game_state: ResMut<NextState<GameState>>,
    game_settings: Res<GameSettings>,
) {
    if input.just_pressed(KeyCode::Enter) {
        for selected_cards in &hand_q {
            if selected_cards.0.len() != game_settings.program_cards_to_chose_ammount.get() as usize
            {
                info!("could not procede to movement phase");
                return;
            }
        }
        game_state.set(GameState::Movement);
    }
}

fn send_move_events(
    mut commands: Commands,
    mut move_event_writer: EventWriter<MoveEvent>,
    mut hand_q: Query<(&mut SelectedCards, &Player), With<Hand>>,
    card_q: Query<&CardKind>,
    tank_q: Query<(Entity, &Player), With<Tank>>,
    time: Res<Time>,
    mut movement_timer: ResMut<MovementTimer>,
    mut game_state: ResMut<NextState<GameState>>,
) {
    movement_timer.0.tick(time.delta());
    if !movement_timer.0.finished() {
        return;
    }
    for (mut selected_cards, player) in &mut hand_q {
        if selected_cards.0.is_empty() {
            game_state.set(GameState::End);
            return;
        }
        let Some(player_entity) = tank_q.iter().find_map(|(entity, tank_player)| {
            if tank_player == player {
                Some(entity)
            } else {
                None
            }
        }) else {
            continue;
        };
        let card = *selected_cards.0.first().unwrap();
        selected_cards.0.remove(0);
        commands.entity(card).remove::<CardSelected>();
        let card_kind = card_q.get(card).unwrap();
        move_event_writer.send(MoveEvent(player_entity, card_kind.0));
    }
}

fn handle_scale_change(time: Res<Time>, mut scalable_q: Query<(&mut Transform, &DesiredScale)>) {
    for (mut transform, desired_scale) in &mut scalable_q {
        let scale = transform.scale;
        transform.scale += (desired_scale.0 - scale) * 30.0 * time.delta_seconds();
    }
}
