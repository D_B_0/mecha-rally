mod card;

use std::{
    f32::consts::{FRAC_PI_2, PI},
    num::NonZeroU8,
};

use bevy::{
    math::{uvec2, vec2},
    prelude::*,
};
use bevy_fast_tilemap::{FastTileMapPlugin, Map, MapBundleManaged, MapIndexer, IDENTITY};
use rand_derive2::RandGen;
use strum_macros::EnumIter;

use crate::AppState;

#[derive(Default, States, Clone, Copy, PartialEq, Eq, Debug, Hash)]
enum GameState {
    #[default]
    NotInGame,
    Preparing,
    Movement,
    End,
}

#[derive(Resource)]
struct GameSettings {
    /// The maximum ammount of cards in the player's hand
    hand_size: NonZeroU8,
    /// The number of cards players have to select before going to the movement phase
    program_cards_to_chose_ammount: NonZeroU8,
    /// The starting ammount of each type of card in the player's deck
    deck_start_composition: fn(Move) -> usize,
}

impl Default for GameSettings {
    fn default() -> Self {
        Self {
            hand_size: NonZeroU8::new(9).unwrap(),
            program_cards_to_chose_ammount: NonZeroU8::new(5).unwrap(),
            deck_start_composition: |card_move| match card_move {
                Move::ForwardOne => 4,
                Move::ForwardTwo => 3,
                Move::ForwardThree => 1,
                Move::Back => 1,
                Move::PowerUp => 1,
                Move::Again => 1,
                Move::RotateRight => 4,
                Move::RotateLeft => 4,
                Move::Rotate180 => 1,
            },
        }
    }
}

pub fn plugin(app: &mut App) {
    app.add_plugins(FastTileMapPlugin::default())
        .init_state::<GameState>()
        .init_resource::<GameSettings>()
        .add_event::<MoveEvent>()
        .add_systems(OnEnter(AppState::Game), (setup, spawn_map, spawn_player))
        .add_systems(
            Update,
            (
                zoom_control_system,
                align_to_map,
                orientation_system,
                player_move_system,
                move_event_system,
            )
                .run_if(in_state(AppState::Game)),
        )
        .add_plugins(card::plugin);
}

const MAP_SIZE_X: u32 = 20;
const MAP_SIZE_Y: u32 = 20;

#[derive(Component)]
struct MapAlignedPosition(UVec2);

impl MapAlignedPosition {
    fn move_n(&mut self, ammount: usize, orientation: Orientation, map_size: UVec2) {
        for _ in 0..ammount {
            match orientation {
                Orientation::North => {
                    if self.0.y != 0 {
                        self.0.y -= 1
                    }
                }
                Orientation::South => {
                    if self.0.y != map_size.y - 1 {
                        self.0.y += 1
                    }
                }
                Orientation::East => {
                    if self.0.x != map_size.x - 1 {
                        self.0.x += 1
                    }
                }
                Orientation::West => {
                    if self.0.x != 0 {
                        self.0.x -= 1
                    }
                }
            }
        }
    }
}

#[derive(Component, Clone, Copy, PartialEq, Eq, Debug)]
enum Orientation {
    North,
    South,
    East,
    West,
}

impl Orientation {
    fn right(&self) -> Orientation {
        match self {
            Orientation::North => Orientation::East,
            Orientation::South => Orientation::West,
            Orientation::East => Orientation::South,
            Orientation::West => Orientation::North,
        }
    }

    fn left(&self) -> Orientation {
        match self {
            Orientation::North => Orientation::West,
            Orientation::South => Orientation::East,
            Orientation::East => Orientation::North,
            Orientation::West => Orientation::South,
        }
    }

    fn back(&self) -> Orientation {
        match self {
            Orientation::North => Orientation::South,
            Orientation::South => Orientation::North,
            Orientation::East => Orientation::West,
            Orientation::West => Orientation::East,
        }
    }
}

type PlayerID = u8;

#[derive(Component, PartialEq, Eq)]
struct Player(PlayerID);

#[derive(Component)]
struct Tank;

#[derive(Event)]
struct MoveEvent(Entity, Move);

#[derive(RandGen, Debug, PartialEq, Eq, Clone, Copy, EnumIter)]
enum Move {
    ForwardOne,
    ForwardTwo,
    ForwardThree,
    Back,
    RotateRight,
    RotateLeft,
    Rotate180,
    PowerUp,
    Again,
}

fn setup(mut game_state: ResMut<NextState<GameState>>) {
    game_state.set(GameState::Preparing);
}

fn spawn_map(
    mut commands: Commands,
    asset_server: Res<AssetServer>,
    mut materials: ResMut<Assets<Map>>,
) {
    let map = Map::builder(
        // Map size
        uvec2(MAP_SIZE_X, MAP_SIZE_Y),
        // Tile atlas
        asset_server.load("tile_atlas.png"),
        // Tile size
        vec2(50.0, 50.0),
    )
    .with_projection(IDENTITY)
    .build_and_initialize(init_map);

    commands.spawn(MapBundleManaged::new(map, materials.as_mut()));
}

fn init_map(m: &mut MapIndexer) {
    for y in 0..m.size().y {
        for x in 0..m.size().x {
            if (x, y) == (0, 0) {
                m.set(x, y, 5);
            } else if (x, y) == (1, 0) {
                m.set(x, y, 3);
            } else if (x, y) == (0, 1) {
                m.set(x, y, 1);
            } else {
                m.set(x, y, 0);
            }
        }
    }
}

fn spawn_player(mut commands: Commands, asset_server: Res<AssetServer>) {
    commands.spawn((
        SpriteBundle {
            texture: asset_server.load("player.png"),
            transform: Transform::from_xyz(0.0, 0.0, 1.0),
            ..default()
        },
        MapAlignedPosition(uvec2(2, 2)),
        Orientation::North,
        Player(0),
        Tank,
    ));
}

fn move_event_system(
    mut move_events: EventReader<MoveEvent>,
    mut movable_query: Query<(&mut Orientation, &mut MapAlignedPosition)>,
    maps: Query<&Handle<Map>>,
    materials: ResMut<Assets<Map>>,
) {
    for MoveEvent(entity, move_enum) in move_events.read() {
        let Ok((mut orientation, mut position)) = movable_query.get_mut(*entity) else {
            continue;
        };
        match move_enum {
            Move::ForwardOne => {
                let Some(map_size) = materials.get(maps.single()).map(Map::map_size) else {
                    continue;
                };
                position.move_n(1, *orientation, map_size)
            }
            Move::ForwardTwo => {
                let Some(map_size) = materials.get(maps.single()).map(Map::map_size) else {
                    continue;
                };
                position.move_n(2, *orientation, map_size)
            }
            Move::ForwardThree => {
                let Some(map_size) = materials.get(maps.single()).map(Map::map_size) else {
                    continue;
                };
                position.move_n(3, *orientation, map_size)
            }
            Move::Back => {
                let Some(map_size) = materials.get(maps.single()).map(Map::map_size) else {
                    continue;
                };
                position.move_n(1, orientation.back(), map_size)
            }
            Move::RotateRight => *orientation = orientation.right(),
            Move::RotateLeft => *orientation = orientation.left(),
            Move::Rotate180 => *orientation = orientation.back(),
            Move::PowerUp => todo!(),
            Move::Again => todo!(),
        }
    }
}

fn zoom_control_system(
    input: Res<ButtonInput<KeyCode>>,
    mut camera_query: Query<&mut OrthographicProjection>,
    time: Res<Time>,
) {
    let mut projection = camera_query.single_mut();

    projection.scale += if input.pressed(KeyCode::Minus) {
        0.6
    } else if input.pressed(KeyCode::Equal) {
        -0.6
    } else {
        0.0
    } * time.delta_seconds();

    projection.scale = projection.scale.clamp(0.2, 5.);
}

fn player_move_system(
    input: Res<ButtonInput<KeyCode>>,
    mut move_event_writer: EventWriter<MoveEvent>,
    tank_q: Query<Entity, With<Tank>>,
) {
    for entity in &tank_q {
        if input.just_pressed(KeyCode::KeyW) {
            move_event_writer.send(MoveEvent(entity, Move::ForwardOne));
        } else if input.just_pressed(KeyCode::KeyS) {
            move_event_writer.send(MoveEvent(entity, Move::Rotate180));
        } else if input.just_pressed(KeyCode::KeyD) {
            move_event_writer.send(MoveEvent(entity, Move::RotateRight));
        } else if input.just_pressed(KeyCode::KeyA) {
            move_event_writer.send(MoveEvent(entity, Move::RotateLeft));
        }
    }
}

fn align_to_map(
    maps: Query<&Handle<Map>>,
    materials: ResMut<Assets<Map>>,
    mut map_aligned_entities: Query<(&mut Transform, &MapAlignedPosition)>,
) {
    // we assume a single map
    let map = materials.get(maps.single()).unwrap();
    for (mut transform, pos) in &mut map_aligned_entities {
        transform.translation = map
            .map_to_local(pos.0.as_vec2() + vec2(0.5, 0.5))
            .extend(transform.translation.z);
    }
}

fn orientation_system(mut orientable_query: Query<(&mut Transform, &Orientation)>) {
    for (mut transform, orientation) in &mut orientable_query {
        transform.rotation = match orientation {
            Orientation::North => Quat::IDENTITY,
            Orientation::South => Quat::from_euler(EulerRot::XYZ, 0.0, 0.0, PI),
            Orientation::East => Quat::from_euler(EulerRot::XYZ, 0.0, 0.0, 3.0 * FRAC_PI_2),
            Orientation::West => Quat::from_euler(EulerRot::XYZ, 0.0, 0.0, FRAC_PI_2),
        }
    }
}

// fn move_marker(
//     mut mouse_events: EventReader<MouseButtonInput>,
//     camera_query: Query<(&GlobalTransform, &Camera), With<OrthographicProjection>>,
//     windows: Query<&Window, With<PrimaryWindow>>,
//     maps: Query<&Handle<Map>>,
//     mut marker: Query<&mut MapAlignedPosition, With<Marker>>,
//     materials: ResMut<Assets<Map>>,
// ) {
//     for event in mouse_events.read() {
//         if event.button != MouseButton::Left || !event.state.is_pressed() {
//             continue;
//         }
//         // we assume a single map
//         let map = materials.get(maps.single()).unwrap();
//         // we assume a single camera
//         let (camera_transform, camera) = camera_query.single();
//         let Some(cursor_position) = windows.single().cursor_position() else {
//             continue;
//         };
//         // Translate viewport coordinates to world coordinates
//         let Some(world_coord) = camera
//             .viewport_to_world(camera_transform, cursor_position)
//             .map(|ray| ray.origin.truncate())
//         else {
//             continue;
//         };
//         // The map can convert between world coordinates and map coordinates
//         let map_coord = map.world_to_map(world_coord).floor();
//         if !(0.0..=(map.map_size().x - 1) as f32).contains(&map_coord.x)
//             || !(0.0..=(map.map_size().y - 1) as f32).contains(&map_coord.y)
//         {
//             continue;
//         }
//         // we assume a single marker
//         let mut marker_pos = marker.single_mut();
//         marker_pos.0 = map_coord.as_uvec2();
//     }
// }
