#![allow(clippy::too_many_arguments)]

mod game;
mod splash;
mod start_menu;

use bevy::{math::vec2, prelude::*, window::PrimaryWindow};

const TEXT_COLOR: Color = Color::rgb(0.9, 0.9, 0.9);

#[derive(Default, States, Clone, Copy, PartialEq, Eq, Debug, Hash)]
enum AppState {
    #[default]
    Splash,
    StartMenu,
    Game,
}

// One of the two settings that can be set through the menu. It will be a resource in the app
#[derive(Resource, Debug, Component, PartialEq, Eq, Clone, Copy)]
struct Volume(u32);

#[derive(Resource)]
struct Cursor(Vec2);

fn main() {
    App::new()
        .insert_resource(Volume(7))
        .init_state::<AppState>()
        .add_systems(Startup, setup)
        .insert_resource(Cursor(vec2(0.0, 0.0)))
        .add_systems(Update, cursor_system)
        .add_plugins((
            DefaultPlugins.set(ImagePlugin::default_nearest()),
            splash::plugin,
            start_menu::plugin,
            game::plugin,
        ))
        .run();
}

fn setup(mut commands: Commands) {
    commands.spawn(Camera2dBundle::default());
}

// Generic system that takes a component as a parameter, and will despawn all entities with that component
fn despawn<T: Component>(to_despawn: Query<Entity, With<T>>, mut commands: Commands) {
    for entity in &to_despawn {
        commands.entity(entity).despawn_recursive();
    }
}

fn cursor_system(
    mut cursor: ResMut<Cursor>,
    window_q: Query<&Window, With<PrimaryWindow>>,
    camera_q: Query<(&Camera, &GlobalTransform)>,
) {
    let (camera, camera_transform) = camera_q.single();
    let window = window_q.single();
    if let Some(world_position) = window
        .cursor_position()
        .and_then(|cursor| camera.viewport_to_world(camera_transform, cursor))
        .map(|ray| ray.origin.truncate())
    {
        cursor.0 = world_position;
    }
}
